---
layout: markdown_page
title: "TMRG - GitLab Women"
description: "An overview of our remote TMRG GitLab Women"
canonical_path: "/company/culture/inclusion/tmrg-gitlab-women/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction



## Mission

The mission of the GitLab Women TMRG is to cultivate an inclusive environment that supports and encourages women to advance their skills and leadership potential through connection, mentorship, collaboration and discussion.  This group shall serve as a forum for women to find their voice and be heard within the GitLab community. Through networking, socializing, and professional development we hope to attract and retain women into GitLab’s positions. This group is open to all members of the GitLab community. 

## Leads
* [Kyla Gradin](https://about.gitlab.com/company/team/#kyla)
* [Madeline Hennessy](https://about.gitlab.com/company/team/) - Co Lead 


## Executive Sponsors
* [Robin Schulman](https://about.gitlab.com/company/team/#rschulman) - Chief Legal Officer and Corporate Secretary
* [Michael McBride](https://about.gitlab.com/company/team/#mmcb) - Chief Revenue Officer

## How To Get Involved
* Please sign up [here](https://docs.google.com/forms/d/1HGYvm8MZ83cN7WAflY4Y6kOehPUUtBk_04cNFt2qtKQ/edit). Once the form is complete you will be added to our monthly meetings. If you have additional questions on how to particpate please reach out Kyla or Madeline to find out more.


## Upcoming Events 
* Women on the Move Blog - More to come!


## Related Performance Indicators and Goals

* [Women at GitLab](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-at-gitlab)
* [GitLab Women in Management - 30% Goal](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-in-management)
* [GitLab Women in Senior Leadership and Executive Roles](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#diversity---women-in-senior-leadership-and-executive-roles)

## Additional Resources

