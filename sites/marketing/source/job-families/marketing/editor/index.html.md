---
layout: job_family_page
title: "Content Editor"
---

## Content Editor

As a Content Editor at GitLab, you will be a part of building the media and publishing
arm of GitLab. You will work closely with team members across the company to develop and optimize stories for the GitLab blog,
and you will use GitLab itself to plan and collaborate. 

### Responsibilities

- Write blog articles with a journalistic approach.
- Cover technology and technical topics as a reporter.
- Remix video, event, and other content into blog articles.
- Proofread, re-structure, and edit article contributions of other team members.
- Interview subject matter experts and industry professionals.
- Curate the bi-weekly newsletter.
- Assist in developing news and announcements as requested within Marketing and by other departments or partners.
- Identify and surface story ideas for placement in external publications.
- Maintain a high standard of well written and factually accurate content.
- Stay up to date on GitLab's current and upcoming products and features.

### Requirements

- Experience as a content writer, editor, or similar role, preferably in enterprise or open source technology marketing.
- Strong communication skills without a fear of [over communication](/handbook/communication/).
- Ability to work [asynchronously](/company/culture/all-remote/asynchronous/)
- Extremely detail-oriented and organized (this applies to editing and writing, as well as to project management of your work, using GitLab) 
- Able to meet deadlines
- Familiarity of the software development process including Git, CI and CD
- Experience with writing for SEO and keyword research.
- Proven ability to copywrite and edit.
- You share our [values](/handbook/values), and work in accordance with those values. For example: in this role, adopting our transparency value sometimes means that your drafts are visible to the rest of the company and teammates might offer input before you're ready!
- Ability to use GitLab
- BONUS: A passion and strong understanding of the industry and our mission.

## Levels

### Associate Content Editor

#### Job Grade

The Associate Content Editor is a [grade 5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Execute writing and editing tasks as assigned by the Managing Editor.
- Learn the enterprise software beat.
- Write and reseach technical topics.

#### Requirements

- 1-3 years experience in a content-related role.
- Ability to meet deadlines and execute on assignments.

### Intermediate Content Editor

#### Job Grade

The Intermediate Content Editor is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Self-manage writing and editing tasks.
- Cover the software & technology beat with a journalistic eye.
- Influence publishing strategy and execution.
- Proven experience with SEO and keyword research.

#### Requirements

- 3-5 years experience in a content-related role.
- Experience writing about software.
- Proven ability to research and write on technical topics independently.

### Senior Content Editor

#### Job Grade

The Senior Content Editor is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities

- Conduct original research and reporting.
- Cultivate story ideas with an astute journalistic eye.
- Influence the strategy and identity of GitLab's digital magazine (e.g. share observations and insights about topic performance, SEO optimization, etc.)
- Assist in newsjacking efforts.

#### Requirements

- 5+ years experience in a content-related role.
- Experience covering technical topics like DevOps, digital transformation, CI/CD, and software development methods.

## Managing Editor

### Job Grade

The Managing Editor is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Responsibilities

- Own the direction and identity of GitLab's digital magazine.
- Set the publishing strategy, directives, and goals.
- Manage the editorial team.
- Oversee all daily publishing operations.
- Report on  and use data to inform publishing strategy.
- Improve and enforce GitLab's editorial style guide, including brand style, tone, and voice.
- Drive improvements to user experience on the GitLab blog.

### Requirements

- 5+ years experience in journalism, content marketing, or communications
- Degree in English, journalism or media, or equivalent work experience.
- Excellent communication, organizational, and leadership capabilities.
- Proven experience as a managing editor or similar role, preferably within enterprise marketing.
- Markdown proficiency.
- Familiarity of the software development processes including Git, CI and CD
- You share our [values](/handbook/values), and work in accordance with those values.
- BONUS: A passion and strong understanding of the industry and our mission.

## Career Progression

The next position in this job family is [Senior Manager, Global Content](/job-families/marketing/global-content-manager/). 
