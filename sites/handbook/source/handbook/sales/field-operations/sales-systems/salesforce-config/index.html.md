---
layout: handbook-page-toc
title: "Salesforce Config"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Salesforce Config
The purpose of this page is to document configuration of our SFDC org. This will serve as the "go-to" place to check in regards to questions on our general configuration.

### Installed Packages
Below is a list of all of our installed packages in SFDC:


| Package Name                                                     | Publisher                               | Owner                          | Department                 | Last Reviewed Date     |
|------------------------------------------------------------------|-----------------------------------------|--------------------------------|----------------------------| -----------------------|
| Marketo Sales Insight                                            | Marketo, Inc.                           | [@darawarde](https://gitlab.com/darawarde)                  | Marketing                  | 5/28/2020
| Field Trip                                                       | RingLead                                | [@jbrennan1](https://gitlab.com/jbrennan1)                         | Sales Systems                  | 8/10/2020
| Bizible Marketing Attribution                                    | Bizible                                 | [@darawarde](https://gitlab.com/darawarde)                      | Marketing                  | 5/28/2020
| ActivityHub                                                      | VersatileCapitalist, Inc                | [@jbrennan1](https://gitlab.com/jbrennan1)                | Sales Ops                  | 5/28/2020
| Salesforce Communities Management                                | Community Analytics                     | [@jbrennan1](https://gitlab.com/jbrennan1)                    | Sales Systems                           | 5/28/2020
| Edit Quotas                                                      | Salesforce.com                          | [@james_harrison](https://gitlab.com/james_harrison)                 | Sales Ops                     | 5/28/2020
| Rollup Helper                                                    | Passage Technology                      | [@jbrennan1](https://gitlab.com/jbrennan1)                   | Sales Systems              | 5/28/2020
| Rollup Helper - Real-Time                                        | Passage Technology                      | [@jbrennan1](https://gitlab.com/jbrennan1)                   | Sales Systems              | 5/28/2020
| Salesforce Connected Apps                                        | Salesforce.com                          | [@jbrennan1](https://gitlab.com/jbrennan1)                   | Sales Systems              | 5/28/2020
| Opportunity Push Counter                                         | salesforce.com                          | [@james_harrison](https://gitlab.com/james_harrison)                 | Sales Ops                       | 5/28/2020
| Mass Lead Converter                                              | Force.com Labs - Mass Lead Converter    | [@darawarde](https://gitlab.com/darawarde)                 | Marketing                           | 5/28/2020
| Impartner PRM (extension)                                        | Impartner Extensions                    | [@cfarris](https://gitlab.com/cfarris)                   | Channel Ops                           | 5/28/2020
| ZoomInfo                                                         | ZoomInfo                                | [@james_harrison](https://gitlab.com/james_harrison)            | Sales Ops                  | 8/10/2020
| Inline Account Hierarchy                                         | FDLC - Inline Account Hierarchy Package | [@james_harrison](https://gitlab.com/james_harrison)                 | Sales Ops                       | 5/28/2020
| Impartner PRM                                                    | Impartner PRM                           |  [@cfarris](https://gitlab.com/cfarris)                            | Channel Ops                           | 5/28/2020
| SalesforceA Connected Apps                                       | SalesforceA Connected Apps              | [@jbrennan1](https://gitlab.com/jbrennan1)                 | Sales Systems                           | 5/28/2020
| ReferenceEdge                                                    | Point of Reference                      | [@kimlock](https://gitlab.com/KimLock)                   | Marketing                           | 8/20/2020
| Demandbase For Salesforce                                        | Demandbase                              | [@emilyluehrs](https://gitlab.com/emilyluehrs)                   | Marketing                  | 5/28/2020
| PathFactory for Sales                                            | PathFactory                             | [@nlarue](https://gitlab.com/nlarue)                    | Marketing                  | 5/28/2020
| Lead Convert Chatter                                             | Force.com Labs                          | [@darawarde](https://gitlab.com/darawarde)                          | Sales Systems             | 5/28/2020
| SN for SFDC                                                      | LinkedIn Sales Navigator                | [@darawarde](https://gitlab.com/darawarde)                      | Marketing                  | 5/28/2020
| Conga Contracts                                                  | Conga Contracts                         | [@rnalen](https://gitlab.com/rnalen)                   | Legal                  | 5/28/2020
| Bizible Marketing Attribution (Multi-Touch Dashboards)           | Bizible                                | [@darawarde](https://gitlab.com/darawarde)                      | Marketing                  | 5/28/2020
| Xactly Express                                                   | Xactly Corp                             | [@james_harrison](https://gitlab.com/james_harrison)  | Sales Ops | 5/28/2020
| Chorus CTI                                                       | AffectLayer Inc                         | [@james_harrison](https://gitlab.com/james_harrison)                 | Sales Ops   | 5/28/2020
| DataFox Orchestrate                                              | DataFox                                 | [@james_harrison](https://gitlab.com/james_harrison_)                 | Sales Ops                  | 5/28/2020
| Visual Compliance RPS Screening                                  | VisualCompliance                        | [@rnalen](https://gitlab.com/rnalen)           | Leagal              | 5/28/2020
| LeanData                                                         | LeanData                                | [@bethpeterson](https://gitlab.com/bethpeterson)                 | Marketing                  | 5/28/2020
| Xactly Express Dashboards and Reports                            | Xactly Corporation                      | [@james_harrison](https://gitlab.com/james_harrison)  | Sales Ops | 5/28/2020
| Salesforce.com CRM Dashboards                                    | Salesforce.com                          | [@jbrennan1](https://gitlab.com/jbrennan1)                               | Sales Systems                           | 5/28/2020
| Salesforce and Chatter Apps                                      | Salesforce.com                          | [@jbrennan1](https://gitlab.com/jbrennan1)                             | Sales Systems                           | 5/28/2020
| DiscoverOrg for Salesforce                                       | DiscoverOrg                             | [@james_harrison](https://gitlab.com/james_harrison)                      | Sales Ops                  | 5/28/2020
| Mass Update And Edit                                             | SFDC                                    | [@james_harrison](https://gitlab.com/james_harrison)                 | Sales Ops                | 5/28/2020
| Zuora for Salesforce 360                                         | Zuora, Inc.                             | [@andrew_murray](https://gitlab.com/andrew_murray)                  | Finance                    | 5/28/2020
| Zuora Quotes                                                     | Zuora                                   | [@andrew_murray](https://gitlab.com/andrew_murray)                   | Finance                    | 5/28/2020
| Gainsight CSM                                                    | Gainsight, Inc.                         | [@jdbeaumont](https://gitlab.com/jdbeaumont)                 | Customer Success           | 5/28/2020
| Sertifi E-Sign for Salesforce.com                                | Sertifi                                 | [@james_harrison](https://gitlab.com/james_harrison)                   | Sales Ops                     | 5/28/2020
| Conga Composer                                                   | AppExtremes                             | [@rnalen](https://gitlab.com/rnalen)                               | Legal                           | 5/28/2020
| Standard Tab Overrides                                           | Software Anywhere                       | [@jbrennan1](https://gitlab.com/jbrennan1)                           | Sales Systems             | 5/28/2020
