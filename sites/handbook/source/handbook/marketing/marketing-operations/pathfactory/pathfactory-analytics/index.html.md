---
layout: handbook-page-toc
title: "PathFactory Analytics"
description: "All roles and permissions have access to reporting functionality in PathFactory."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## PathFactory analytics

All roles and permissions have access to reporting functionality in PathFactory. Feel free to peruse `Path Analytics` or view insights in the content library and content tracks. If you would like a scheduled report or have a different PathFactory data request, create an issue in the Marketing Operations project. All of the below links lead to PathFactory's knowledgebase, _The Nook_. The Nook requires a separate password from PathFactory but you should use the same email address used to login to PathFactory.

- [Glossary](https://lookbookhq.force.com/nook/s/article/glossary)
- [Diving into content analytics](https://customer.pathfactory.com/success-series/youtube-3?lx=v-9_uV&search=analytics)

### Target track analytics

1. [Analytics for a Specific Target Track](https://lookbookhq.force.com/nook/s/article/analytics-for-a-specific-target-track)
1. [Analytics for All Target Tracks](https://lookbookhq.force.com/nook/s/article/analytics-for-all-target-tracks)

### Recommended track analytics

1. [Analytics for a Specific Recommend Track](https://lookbookhq.force.com/nook/s/article/analytics-for-a-specific-recommend-track)
1. [Analytics for All Recommend Tracks](https://lookbookhq.force.com/nook/s/article/analytics-for-all-recommend-tracks)

### Explore page analytics

1. [Explore Page Analytics](https://lookbookhq.force.com/nook/s/article/explore-page-analytics)

### ABM analytics

1. [Understanding Account Based Analytics](https://lookbookhq.force.com/nook/s/article/understanding-account-based-analytics)
1. [Understanding Route Analytics](https://lookbookhq.force.com/nook/s/article/understanding-route-analytics)

### Website promoter analytics

1. [Website Promoter Analytics](https://lookbookhq.force.com/nook/s/article/website-promoter-analytics)

### Defining visitor activities

1. [Understanding Visitor Activities](https://lookbookhq.force.com/nook/s/article/understanding-visitor-activities)

### Path analytics

1. [Introducing: Path Analytics](https://lookbookhq.force.com/nook/s/article/path-analytics-intro)
1. [Path Analytics: Overview Dashboard](https://lookbookhq.force.com/nook/s/article/pa-overview)
1. [Path Analytics: Visitors](https://lookbookhq.force.com/nook/s/article/pa-visitors)
1. [Path Analytics: Accounts](https://lookbookhq.force.com/nook/s/article/pa-accounts)
1. [Path Analytics: Content](https://lookbookhq.force.com/nook/s/article/pa-content)
1. [Path Analytics FAQ](https://lookbookhq.force.com/nook/s/article/path-analytics-faq)
1. [Using and Sharing Path Analytics Reports](https://lookbookhq.force.com/nook/s/article/looker-overview)
1. [Types of Path Analytics Reports](https://lookbookhq.force.com/nook/s/article/looker-reports)
1. [Path Analytics Reports FAQ](https://lookbookhq.force.com/nook/s/article/looker-reports-faq)

### Using account-based analytics

1. [Using Account Based Analytics](https://lookbookhq.force.com/nook/s/article/account-based-analytics)