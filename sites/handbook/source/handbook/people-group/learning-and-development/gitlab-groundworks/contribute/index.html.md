---
layout: handbook-page-toc
title: Contribute to GitLab Groundworks
---

# Contribute to GitLab Groundworks

## Handbook First Learning Content

Content in the LXP is created using a handbook first approach to learning. This means that all learning content that users can access in the LXP can be found in the handbook. A [handbook first approach to learning](/handbook/people-group/learning-and-development/interactive-learning) in the LXP ensures:

- all voices and contributions are heard
- barriers to contributions are removed or reduced
- the organization maintains a single sorce of truth

The steps for [how to contribute content to the LXP](/handbook/people-group/learning-and-development/#how-to-contribute) outlines examples of how users can create handbook first learning content.


## Learning content in the LXP

EdCast organizes content in the LXP in three major types of content buckets. When contributing to the LXP, it's important to consider which of these three types might be best to organize content and guide learners to their learning objectives.

### SmartCards

SmartCards are the basic unit of information in the LXP. They are used to direct learners to explore specific pieces of content in the handbook, watch recorded videos, or complete quick knowledge checks and assessments. Content in the LXP is built and arranged with SmartCards.

#### Examples of SmartCard content

- Links to GitLab handbook pages, documentation pages, and YouTube videos
- Links to external articles and resources that can supplement handbook resources
- Advertising/sharing a live Learner Speaker Series being hosted for GitLab team members, or a public AMA with our Product team. These can also be used for members to sign up to attend live learning sessions
- Polling a group of users to get their feedback on an interactive course on Emotional Intelligence
- Assessing learnings with a short quiz/knowledge check after reading a set of handbook pages
- Space for learners to upload and share submissions to projects, like a slide deck or animated video they've created

### Pathway

A Pathway is a collection of SmartCards organized as a learning path.

#### Examples of Pathway content

- A set of 6 handbook pages (SmartCards) readers should review in sequence to gain a stronger understanding of one topic

### Journey

A Journey is a structured sequence of SmartCards and Pathways.

#### Examples of Journey content

- A set of multiple pathways a user must work through in order to earn a certification, for example GitLab 101 or GitLab 201


## Organizing content in the LXP

Content in the LXP is organized using the following structures. Learners use these structures to discover new content.

### Carousels

Carousels are horizontal containers of related channels or content assets.

### Examples of Carousel organization

- A carousel could be curated to display 3 different journey's related to Emotional Intelligence

### Channels

Channels are the principal way in which content (SmartCards, Pathways, and Journeys) are broadcast to learners throughout the LXP. Channels can be customizable based on the vision of the learners using the LXP. 

#### Examples of Channel organization

- A series of SmartCards, Pathways, and Journeys on Agile Project Management

### Group

Groups are a collection of people with a similar characteristic or learning interest.

#### Examples of Group organization

- The Marketing team organizes a group of program managers to participate in a specific journey


## How to Contribute

The GitLab team uses GitLab to organize and collaborate on the contribution of content to the LXP. All related issues can be found in the [LXP Contribution GitLab project](https://gitlab.com/gitlab-com/people-group/learning-development/lxp-contributions), managed by the L&D team.

### Step 1: Create and Curate Learning Content

1. Make a contribution to the handbook. All learning content in the LXP is built with a handbook first approach. This means that information you'd like to add to the LXP must be stored first in the handbook. If you have not made a contribution to the GitLab handbook before, please review [how to contribute to GitLab](https://about.gitlab.com/community/contribute/). 

The table below should be used as a reference for suggested handbook sections where learning content can be uploaded. If you aren't sure where to contribute learning content in the handbook, please review the [main handbook page](https://about.gitlab.com/handbook/#introduction) or contact the Learning and Development team at `learning@gitlab.com` for support.

| Content | Suggested Handbook Location |
| ----- | ----- |
| Leadership: emotional intelligence, coaching, and all other factors that contribute to growing and developing as a leader at GitLab | [Leadership Handbook](https://about.gitlab.com/handbook/leadership/) |
| Communication: effective communication strategies, remote commiunication, and any additional content that contributes to strengthening asynchronous and remote communication skills | [Communication Handbook](https://about.gitlab.com/handbook/communication/) |
| Diversity, Inclusion, and Belonging: inclusivity trainings, strategies for developing a sense of belonging, and content related to implementing DIB initiatieves | [DIB Handbook](https://about.gitlab.com/company/culture/inclusion/) | 

1. If the content you'd like to contribute to the LXP is already included in the handbook - great! Your contribution to the LXP could be a new or creative curation of existing content from GitLab that helps a learner better understand a topic or reach a goal. Consider curating current content from the handbook, [GitLab YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), and [GitLab documentation](https://docs.gitlab.com/). Consider taking the opportunity to add to or update existing content before contributing to the LXP.

### Step 2: Propose Content to be included in the LXP

1. Using the descriptions above, decide which content type might be the best fit for the information you'd like to contribute. Navigate to the corresponding creator kit issue templates below and begin to populate the issue with required content.

Issue Templates:

- SmartCard Creator Kit
- Pathway Creator Kit
- Journey Creator Kit
- Carousel Creator Kit
- Channel Creator Kit
- Group Creator Kit

**Note:** In order for content contributions to be reviewed by the appropriate GitLab team, all information on the issue template must be addressed. GitLab team members may ping you to address content gaps or answer questions about your contributions. 

### Step 3: Iterate on content with GitLab team via issues

1. After all required content is uploaded to the correct creator kit issue, GitLab team members will collaborate with contributors to iterate on and refine contributions. The following chart highlights which GitLab teams will collaborate on and approve content contributed to the LXP:

| Contributor Audience | Team Approval | GitLab Label|
| ----- | ----- | ----- |
| GitLab Team Members | L&D Team | `edcast-contribution-l&D` |
| Customers | Professional Services | `edcast-contribution-professional-services` |
| Partners | Partner Enablement | `edcast-contribution-partners` |
| Sales Team | Field Enablement | `edcast-contribution-field` |
| Wider GitLab Community | Community Relations Team | `edcast-contribution-community` |


To learn more about the admin review and approval process of contributions to the LXP, please review the LXP admin section of the handbook.

1. The GitLab team will determine when your contributions are ready to be added to the LXP and will communicate with contributors on related issues.


### Step 4: Content is uploaded to the LXP

1. The GitLab team members outlined above will upload related content to the LXP. Additional context about this admin process can be found in the LXP admin section of the handbook.

**Note:** At the GitLab team's discretion, individual contributors may be given permissions to upload content to the LXP.
